package com.dochajj.provider.Utilities;

import android.util.Log;

/**
 * Created by Freeware Sys on 4/8/2017.
 */

public class Utilities {

    boolean showLog = true;

    public void print(String tag, String message) {
        if(showLog){
            Log.v(tag,message);
        }
    }
}
